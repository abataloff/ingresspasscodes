﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressPasscodes
{
    interface ICodeReceivingMethod
    {
        event EventHandler<CodeReceivedEventArgs> CodeReceived;

        event EventHandler StateChanged;

        MethodState State { get; }
        
        void Start();

        void Stop();

        string Name { get;}
    }
}
