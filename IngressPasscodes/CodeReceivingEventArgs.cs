﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IngressPasscodes
{
    class CodeReceivedEventArgs:EventArgs
    {
        public CodeReceivedEventArgs(string[] a_code)
        {
            Code = a_code;
        }

        internal string[] Code{get;private set;}
    }
}
