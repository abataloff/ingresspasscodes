﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressPasscodes
{
    class AddressesFromFileProvider:IAddressProvider
    {
        string filePath;
        public AddressesFromFileProvider(string a_filePath)
        {
            filePath = a_filePath;
        }

        public Uri[] GetAddresses()
        {
            List<Uri> _retLis = new List<Uri>();
            string[] _strings = BadClass.ReadStringsFormFile(filePath);
            foreach (var _address in _strings)
            {
                Uri _uri = new Uri(_address);
                if (!_retLis.Contains(_uri))
                    _retLis.Add(_uri);
            }
            return _retLis.ToArray();
        }
    }
}
