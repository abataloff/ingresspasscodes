﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IngressPasscodes
{
    public class IngressPasscodesNotifyIcon
    {
        public event EventHandler ExitClick;

        NotifyIcon notifyIcon;
        ToolStripMenuItem tsmiCodeReceivedMethods;

        public IngressPasscodesNotifyIcon()
        {
            notifyIcon = new NotifyIcon();

            // Настройка визуального оформления
            notifyIcon.Icon = Properties.Resources.NotifyIcon;
            notifyIcon.Visible = true;

            // Добавление контекстного меню
            notifyIcon.ContextMenuStrip = new ContextMenuStrip();

            tsmiCodeReceivedMethods = new ToolStripMenuItem("Способы получения кода");
            notifyIcon.ContextMenuStrip.Items.Add(tsmiCodeReceivedMethods);

            notifyIcon.ContextMenuStrip.Items.Add(new ToolStripSeparator());

            ToolStripMenuItem _miExit = new ToolStripMenuItem("Выход");
            _miExit.Click +=delegate(object sender, EventArgs e)
            {
                doExitClick();
            };
            notifyIcon.ContextMenuStrip.Items.Add(_miExit);
        }

        internal void AddCodeReceivedMethod(params ICodeReceivingMethod[] a_codeReceivedMethods)
        {
            List<ToolStripMenuItem> _lisItems = new List<ToolStripMenuItem>();
            foreach (var _crm in a_codeReceivedMethods)
            {
                ToolStripMenuItem _mi = createMenuItem(_crm);
                if (_mi != null)
                    _lisItems.Add(_mi);
            }
            tsmiCodeReceivedMethods.DropDownItems.AddRange(_lisItems.ToArray());
        }

        static ToolStripMenuItem createMenuItem(ICodeReceivingMethod a_crm)
        {
            ToolStripMenuItem _retMI = new ToolStripMenuItem (a_crm.Name);
            _retMI.CheckOnClick = true;

            // Задаем начальное значение
            _retMI.Checked = a_crm.State == MethodState.Start;

            EventHandler _eh = delegate(object sender, EventArgs e)
            {
                if (_retMI.Checked)
                {
                    a_crm.Start();
                }
                else
                {
                    a_crm.Stop();
                }
            };

            _retMI.CheckedChanged += _eh;

            return _retMI;
        }
        
        private void doExitClick()
        {
            if (ExitClick != null)
                ExitClick.Invoke(this, new EventArgs());
        }
    }
}
