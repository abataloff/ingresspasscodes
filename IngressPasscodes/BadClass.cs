﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IngressPasscodes
{
    internal class BadClass
    {

        public const string WEBADDRESSES_FILE = "addresses";
        private void writeNewCodes(string[] _newCodes)
        {
            throw new NotImplementedException();
        }

        private string[] readOldCodes()
        {
            return ReadOldCodesFromFile(Properties.Settings.Default.CodesFile);
        }

        public static string[] getWebAddresses(string WEBADDRESSES_FILE)
        {
            return ReadStringsFormFile(WEBADDRESSES_FILE);
        }

        public static void WriteNewCodesToFile(string a_fnfCodes, string[] a_newCodes)
        {
            writeStringsToFile(a_fnfCodes, a_newCodes);
        }

        public static string[] getNewCodes(string[] a_codesFromPage, string[] a_oldCodes)
        {
            List<string> _retLis = new List<string>();
            List<string> _oldCodes = new List<string>((IEnumerable<string>)a_oldCodes);
            foreach (var _code in a_codesFromPage)
            {
                if (!_oldCodes.Contains(_code))
                    _retLis.Add(_code);
            }
            return _retLis.ToArray();
        }

        public static string[] ReadOldCodesFromFile(string a_fnf)
        {
            return ReadStringsFormFile(a_fnf);
        }


        public static string[] ReadStringsFormFile(string a_fnf)
        {
            List<string> _retVal = new List<string>();
            if (File.Exists(a_fnf))
            {
                TextReader _tr = new StreamReader(File.OpenRead(a_fnf));
                string _str = _tr.ReadLine();
                if (!string.IsNullOrEmpty(_str))
                    _retVal.Add(_str);
                while (_str != null)
                {
                    _str = _tr.ReadLine();
                    if (!string.IsNullOrEmpty(_str))
                        _retVal.Add(_str);
                }
                _tr.Close();
            }
            return _retVal.ToArray();
        }

        static void writeStringsToFile(string a_fnf, params string[] a_strings)
        {
            TextWriter _tw = new StreamWriter(a_fnf, true);
            foreach (var _code in a_strings)
            {
                _tw.WriteLine(_code);
            }
            _tw.Close();
        }

    }
}
