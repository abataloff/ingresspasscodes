﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IngressPasscodes
{
    class Loger
    {
        static Loger codes = new Loger("codes.log");
        static Loger error = new Loger("error.log");
        private string fnf;

        public Loger(string a_fnf)
        {
            fnf = a_fnf;
        }
     
        internal static Loger Codes
        {
            get
            {
                return codes;
            }
        }

        internal static Loger Error
        {
            get
            {
                return error;
            }
        }

        internal void Add(string a_text)
        {
            TextWriter _tw = new StreamWriter(fnf, true);
            _tw.WriteLine(a_text);
            _tw.Close();
        }
    }
}
