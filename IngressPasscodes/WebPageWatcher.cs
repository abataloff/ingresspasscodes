﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IngressPasscodes
{
    class WebPageWatcher:ICodeReceivingMethod
    {
        const string NAME = "С Web страниц";
        const int INTERVAL = 30 * 1000;
        Timer _timer;

        public event EventHandler<CodeReceivedEventArgs> CodeReceived;

        public event EventHandler StateChanged;

        MethodState h_state = MethodState.Stop;

        private IAddressProvider addressesProvider;

        public MethodState State
        {
            get 
            {
                return h_state;
            }
            private set
            {
                if (h_state != value)
                {
                    h_state = value;
                    doStateChanged();
                }
            }
        }

        public WebPageWatcher(IAddressProvider a_addressesProvider)
        {
            addressesProvider = a_addressesProvider;            
        }

        private void doStateChanged()
        {
            if (StateChanged != null)
                StateChanged.Invoke(this, new EventArgs());
        }

        public void Start()
        {
            watch();
            _timer = new Timer();
            _timer.Interval = INTERVAL;
            _timer.Tick += _timer_Tick;
            _timer.Start();
            State = MethodState.Start;
        }

        public void Stop()
        {
            _timer.Stop();
            State = MethodState.Stop;
        }

        public string Name
        {
            get
            {
                return NAME;
            }
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            try
            {
                watch();
            }
            catch (Exception _ex)
            {
                Loger.Error.Add(_ex.ToString());
            }
        }

        private void watch()
        {
            // Получаем текст страниц.
            string _pagesText = string.Empty;
            Uri[] _webAddresses = addressesProvider.GetAddresses();
            foreach (var _webAddress in _webAddresses)
            {
                _pagesText += getPageText(_webAddress);
            }

            // Получаем коды со страниц.
            string[] _codesFromPages = getCodesFormText(_pagesText);

            if (_codesFromPages.Length > 0)
                doCodeReceived(new CodeReceivedEventArgs(_codesFromPages));
        }

        private void doCodeReceived(CodeReceivedEventArgs a_codeReceivedEventArgs)
        {
            if (CodeReceived != null)
                CodeReceived.Invoke(this, a_codeReceivedEventArgs);
        }

        string getPageText(Uri a_uri)
        {
            WebClient _client = new WebClient();

            byte[] _buffer = _client.DownloadData(a_uri);

            return Encoding.UTF8.GetString(_buffer);
        }

        public static string[] getCodesFormText(string _pageText)
        {
            List<string> _retList = new List<string>();
            Regex _reg = new Regex("[0-9][a-z][a-z][0-9]{1,2}[a-z]{2,}[a-z][0-9][a-z][0-9][a-z]");
            foreach (var _match in _reg.Matches(_pageText))
            {
                string _code = _match.ToString();
                if (!_retList.Contains(_code))
                    _retList.Add(_code);
            }
            _reg = new Regex("[P|p]asscode: [a-z]+");
            foreach (var _match in _reg.Matches(_pageText))
            {
                string _code = _match.ToString().Remove(0, 10);
                if (!_retList.Contains(_code))
                    _retList.Add(_code);
            }
            return _retList.ToArray();
        }   
    }
}
