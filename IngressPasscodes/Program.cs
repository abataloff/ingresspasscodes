﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IngressPasscodes
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // Получаем множестов "получателей кодов".
            ICodeReceivingMethod[] _crMethods = new ICodeReceivingMethod[] { 
                new WebPageWatcher(new AddressesFromFileProvider(BadClass.WEBADDRESSES_FILE)),
            };
            
            // Настраиваем обработчик получения кодов.
            EventHandler<CodeReceivedEventArgs> _ehCodeReceived =
                new EventHandler<CodeReceivedEventArgs>(codeReceived);

            foreach (var _crm in _crMethods)
            {
                _crm.CodeReceived += _ehCodeReceived;
            }

            // Создаем пользовательский интерфейс (иконка в обасти уведомлений).
            CreateGUI(_crMethods);

            Application.Run();
        }

        private static void codeReceived(object sender, CodeReceivedEventArgs e)
        {
            MessageBox.Show(string.Join("\n", e.Code));
        }

        private static void CreateGUI(ICodeReceivingMethod[] _crMethods)
        {
            // Создаем иконку в области уведомлений.
            IngressPasscodesNotifyIcon _icon = new IngressPasscodesNotifyIcon();

            // Добавляем способы получения кодов.
            _icon.AddCodeReceivedMethod(_crMethods);

            // Добавляем обработчик нажатия кнопки "Выход".
            _icon.ExitClick += new EventHandler(_icon_ExitClick);
        }

        private static void _icon_ExitClick(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}